package com.koval.synchronize;

import org.apache.logging.log4j.*;
import java.util.concurrent.locks.ReentrantLock;

public class Synchronize {

    private static final ReentrantLock lock = new ReentrantLock();
    private static long adder = 0;
    private static final Logger logger = LogManager.getLogger(Synchronize.class);

    private void method1() {
        logger.trace(Thread.currentThread().getName() + " is running first method\n");
        for (int i = 0; i < 1000000; i++) {
            adder++;
        }
        logger.trace("Value after first method: " + adder + "\n");
    }

    private void method2() {
        logger.trace(Thread.currentThread().getName() + " is running second method\n");
        for (int i = 0; i < 1000000; i++) {
            adder++;
        }
        logger.trace("Value after second method: " + adder + "\n");
    }

    private void method3() {
        logger.trace(Thread.currentThread().getName() + " is running third method\n");
        for (int i = 0; i < 1000000; i++) {
            adder++;
        }
        logger.trace("Value after third method: " + adder + "\n");
    }

    public void invoke(){
        lock.lock();
        method1(); method2(); method3();
        lock.unlock();
    }
}
