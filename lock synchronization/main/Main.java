
package com.koval;

import com.koval.synchronize.Synchronize;

public class Main {

    public static void main(String[] args) {
        Synchronize synchronize = new Synchronize();

        Thread t1 = new Thread(synchronize::invoke);
        Thread t2 = new Thread(synchronize::invoke);
        Thread t3 = new Thread(synchronize::invoke);

        t1.start();
        t2.start();
        t3.start();
    }
}


