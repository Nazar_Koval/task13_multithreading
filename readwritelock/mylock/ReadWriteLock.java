package com.koval.customlock;

public class ReadWriteLock {

    private static final int onWrite = -1;
    private Thread current;
    private int rAmount = 0;

    public synchronized void rLock() throws InterruptedException {
            while (rAmount == onWrite) wait();
            rAmount++;
    }

    public synchronized void wLock() throws InterruptedException {
            while (rAmount != 0)wait();
            rAmount = onWrite;
            current = Thread.currentThread();
    }

    public synchronized void rUnlock(){
            if(rAmount > 0) {
                rAmount--;
                if (rAmount == 0) notifyAll();

            }
    }

    public synchronized void wUnlock(){
        if(rAmount == onWrite && current == Thread.currentThread()) {
            current = null;
            rAmount = 0;
            notifyAll();
        }
    }
}
