package com.koval;

import com.koval.customlock.ReadWriteLock;
import org.apache.logging.log4j.*;

public class Main {

    private static Logger logger = LogManager.getLogger(Main.class);
    private static long checker = 0;
    private static ReadWriteLock readWriteLock = new ReadWriteLock();

    public static void main(String[] args) throws InterruptedException {

        Thread thread = new Thread(() ->{

            try {
                    Thread.sleep(500);
                    readWriteLock.rLock();
                for (int i = 0; i < 10; i++) {
                    Thread.sleep(500);
                    checker++;
                    logger.trace("T1" + " ");
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            finally {
                readWriteLock.rUnlock();
            }
        },"T1");

        Thread thread1 = new Thread(()->{

            try {
                readWriteLock.wLock();
                for (int i = 0; i < 10; i++) {
                    Thread.sleep(1000);
                    checker++;
                    logger.trace("T2" + " ");
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }finally {
                readWriteLock.wUnlock();
            }

        },"T2");

        thread.start(); thread1.start();
        Thread.sleep(17000);
        logger.trace("\nValue: " + checker);
    }
}

