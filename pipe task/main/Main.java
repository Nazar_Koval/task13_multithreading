package com.koval;

import org.apache.logging.log4j.*;
import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;

public class Main {

    private static Logger logger = LogManager.getLogger(Main.class);

    public static void main(String[] args)throws Exception {

        final PipedOutputStream output = new PipedOutputStream();
        final PipedInputStream input  = new PipedInputStream();
        input.connect(output);

        Thread writeThread = new Thread(() -> {
            try {

            for (int i = 0; i < 33 ; i++) {
                output.write((byte) (i + 1));
                logger.trace(Thread.currentThread().getName() + " writing: " + (i+1) + "\n");
            }

            output.close();
            }catch (IOException e){e.printStackTrace();}

        });

        Thread readThread = new Thread(() -> {
            try {
                int c;

                while ((c = input.read()) != -1){
                    logger.trace(Thread.currentThread().getName() + " reading: " + c + "\n");
                }

                input.close();
            }catch (IOException e){e.printStackTrace();}

        });

        writeThread.start(); readThread.start();
    }
}

