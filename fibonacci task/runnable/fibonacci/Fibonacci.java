package com.koval.fibonacci;

import org.apache.logging.log4j.*;

public class Fibonacci implements Runnable {

    private static Logger logger = LogManager.getLogger(Fibonacci.class);
    private int n;
    private int[]fib;

    public Fibonacci(int n){
        this.n = n;
        fib = new int[n];
    }

    @Override
    public void run() {
        fib[0] = 0;
        fib[1] = 1;
        logger.trace("Fibonacci number #" + 1 + ": " + fib[0] + "\n");
        logger.trace("Fibonacci number #" + 2 + ": " + fib[1] + "\n");
        int i = 2;

        while (i < n){
            fib[i] = fib[i-1] + fib[i-2];
            int tmp = i+1;
            logger.trace("Fibonacci number #" + tmp + ": " + fib[i] + "\n");
            i++;
        }

        logger.trace("\nEnd of Fibonacci thread\n");
    }
}
