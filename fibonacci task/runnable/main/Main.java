package com.koval;

import com.koval.fibonacci.Fibonacci;
import org.apache.logging.log4j.*;
import java.util.Scanner;
import java.util.concurrent.*;

public class Main {

    private static Logger logger = LogManager.getLogger(Main.class);
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {

        logger.trace("Choose an option:\n");
        logger.trace("1 - Ordinary starting\n" +
                "2 - Starting with executor service\n" +
                "3 - Starting with a 5-second schedule\n" +
                "4 - 5-time execution with 3-second intervals\n");

        logger.trace("\nInput chosen option: ");
        int option = scanner.nextInt();

        switch (option) {

            case 1:
                logger.trace("Ordinary starting\n");
                Thread thread = new Thread(new Fibonacci(30));
                thread.start();
                break;

            case 2:
                logger.trace("Starting with executor service\n");
                ExecutorService executorService = Executors.newSingleThreadExecutor();
                executorService.execute(new Thread(new Fibonacci(30)));
                executorService.shutdown();
                break;

            case 3:
                logger.trace("Starting with a 5-second schedule\n");
                ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);
                ScheduledFuture<?> future = executor.schedule(new Thread
                        (new Fibonacci(30)), 5, TimeUnit.SECONDS);

                while (future.getDelay(TimeUnit.SECONDS) > 0) {
                    logger.trace("Please wait...\n");
                }

                logger.trace("Now we`re starting\n");
                executor.shutdown();
                break;

            case 4:
                logger.trace("5-time execution with 3-second intervals");
                ScheduledExecutorService singleScheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
                singleScheduledExecutorService.scheduleAtFixedRate(new Thread
                        (new Fibonacci(30)), 1, 3, TimeUnit.SECONDS);
                long constrait = 15000;
                long start = System.currentTimeMillis();
                while (constrait > System.currentTimeMillis() - start) {
                }
                singleScheduledExecutorService.shutdown();
                break;

            default:
                logger.trace("Wrong input");
                break;
        }
    }
}
