package com.koval.fibonacci;

import org.apache.logging.log4j.*;
import java.util.concurrent.Callable;

public class Fibonacci implements Callable<Long> {

    private static Logger logger = LogManager.getLogger(Fibonacci.class);
    private int n;
    private int[]fib;

    public Fibonacci(int n){
        this.n = n;
        fib = new int[n];
    }

    @Override
    public Long call() {

        long sum = 0L;
        fib[0] = 0;
        fib[1] = 1;
        logger.trace("Fibonacci number #" + 1 + ": " + fib[0] + "\n");
        logger.trace("Fibonacci number #" + 2 + ": " + fib[1] + "\n");

        sum += fib[0] + fib[1];
        int i = 2;

        while (i < n){
            fib[i] = fib[i-1] + fib[i-2];
            int tmp = i+1;
            logger.trace("Fibonacci number #" + tmp + ": " + fib[i] + "\n");
            sum += fib[i];
            i++;
        }

        logger.trace("\nEnd of Fibonacci thread\n");
        return sum;
    }
}
