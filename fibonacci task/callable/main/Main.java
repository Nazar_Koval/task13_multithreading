package com.koval;

import com.koval.fibonacci.Fibonacci;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class Main {

    private static Logger logger = LogManager.getLogger(Main.class);

    public static void main(String[] args) {

        ExecutorService executorService = Executors.newSingleThreadExecutor();
        Future<Long>longFuture = executorService.submit(new Fibonacci(30));

        logger.trace("Part #1\n");
        try {
            logger.trace("\nSum of all the generated Fibonacci numbers in first part: " + longFuture.get() + "\n");
        }catch (InterruptedException | ExecutionException e){
            e.printStackTrace();
        }

        executorService.shutdown();

        logger.trace("\nPart #2\n");
        executorService = Executors.newSingleThreadExecutor();
        longFuture = executorService.submit(new Fibonacci(15));

        try {
            logger.trace("\nSum of all the generated Fibonacci numbers in second part: " + longFuture.get() + "\n");
        }catch (InterruptedException | ExecutionException e){
            e.printStackTrace();
        }

        executorService.shutdown();
    }
}
