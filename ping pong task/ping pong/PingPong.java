package com.koval.pingpong;

import org.apache.logging.log4j.*;

public class PingPong {

    private static final Object monitor = new Object();
    private static Logger logger = LogManager.getLogger(PingPong.class);

    @SuppressWarnings("Duplicates")
    public void pingpong(){

        Thread first = new Thread(()-> {synchronized (monitor){
            logger.trace(Thread.currentThread() + " started playing game\n");
            int i = 0;
            while (i < 10) {
                try {
                    monitor.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                i++;
                logger.trace("ping\n");
                monitor.notify();
            }
            logger.trace(Thread.currentThread() + " finished playing game\n");
        } });

        Thread second = new Thread(() ->{synchronized (monitor) {
            logger.trace(Thread.currentThread() + " started playing game\n");
            int i = 0;
            while (i < 10) {
                monitor.notify();
                try {
                    monitor.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                i++;
                logger.trace("pong\n");
            }
            logger.trace(Thread.currentThread() + " finished playing game\n");
        } });

        first.start();
        second.start();
    }
}
