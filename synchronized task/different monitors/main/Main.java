package com.koval;

import com.koval.synchronize.Synchronize;
import org.apache.logging.log4j.*;

public class Main {

    private static Logger logger = LogManager.getLogger(Main.class);

    public static void main(String[] args) {
        Synchronize synchronize = new Synchronize();

        Thread t1 = new Thread(synchronize::invoke);
        Thread t2 = new Thread(synchronize::invoke);
        Thread t3 = new Thread(synchronize::invoke);

        t1.start();
        t2.start();
        t3.start();

        try {
            t1.join();
            t2.join();
            t3.join();
        }catch (InterruptedException e){e.printStackTrace();}

        logger.trace("\nFinal value: " + Synchronize.adder);
    }
}
