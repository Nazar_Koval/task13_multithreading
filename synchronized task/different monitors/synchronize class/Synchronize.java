package com.koval.synchronize;

import org.apache.logging.log4j.*;

public class Synchronize {

    private static final Object monitor1 = new Object();
    private static final Object monitor2 = new Object();
    private static final Object monitor3 = new Object();
    public static long adder = 0;
    private static Logger logger = LogManager.getLogger(Synchronize.class);

    private void method1() {
        synchronized (monitor1) {
            logger.trace(Thread.currentThread().getName() + " is running first method\n");
            for (long i = 0; i < 1000000000; i++) {
                adder++;
            }
            logger.trace("Value after first method: " + adder + "\n");
        }
    }

    private void method2() {
        synchronized (monitor2) {
            logger.trace(Thread.currentThread().getName() + " is running second method\n");
            for (int i = 0; i < 1000000000; i++) {
                adder++;
            }
            logger.trace("Value after second method: " + adder + "\n");
        }
    }

    private void method3() {
        synchronized (monitor3) {
            logger.trace(Thread.currentThread().getName() + " is running third method\n");
            for (int i = 0; i < 1000000000; i++) {
                adder++;
            }
            logger.trace("Value after third method: " + adder + "\n");
        }
    }

    public void invoke(){
        method1(); method2(); method3();
    }
}
