package com.koval.sleep;

import org.apache.logging.log4j.*;
import java.util.Random;

public class SleepTask implements Runnable {

    private static Logger logger = LogManager.getLogger(SleepTask.class);

    @Override
    public void run() {

        Random rd = new Random();
        int time = rd.nextInt(10) + 1;
        logger.trace("Current thread: " + Thread.currentThread() + " starts sleeping for " + (time*1000) +" milliseconds\n");

        try {
            Thread.sleep(time * 1000);
        }catch (InterruptedException e){
            e.printStackTrace();
        }
        logger.trace("End of the " + Thread.currentThread() + "\n");
    }
}
