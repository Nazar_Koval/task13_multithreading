package com.koval;

import com.koval.sleep.SleepTask;
import org.apache.logging.log4j.*;
import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class Main {

    private static Logger logger = LogManager.getLogger(Main.class);

    public static void main(String[] args) {

        Random rd = new Random();
        int threads = Integer.parseInt(args[0]);
        ScheduledExecutorService executorService = Executors.newScheduledThreadPool(threads);

        for (int i = 0; i < threads; i++) {
            Thread current = new Thread(new SleepTask());
            ScheduledFuture<?> future = executorService.schedule(current,rd.nextInt(6)+1, TimeUnit.SECONDS);

            logger.trace(current.getName() + " starts execution after "
                    + future.getDelay(TimeUnit.SECONDS) + "-second delay\n");
        }
        executorService.shutdown();
    }
}
