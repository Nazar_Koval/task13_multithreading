package com.koval;

import org.apache.logging.log4j.*;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;

public class Main {

    private static Logger logger = LogManager.getLogger(Main.class);
    private static BlockingQueue<Integer>integerBlockingQueue = new LinkedBlockingDeque<>();

    public static void main(String[] args){

        Thread writeThread = new Thread(() -> {
            for (int i = 0; i < 33; i++) {
                try {
                    integerBlockingQueue.put(i + 1);
                    logger.trace(Thread.currentThread().getName() + " writing: " + (i + 1) + "\n");
                    Thread.sleep(750);
                }catch (InterruptedException e){e.printStackTrace();}
            }
        },"T1");

        Thread readThread = new Thread(() -> {
            for (int i = 0; i < 33; i++) {
                try {
                    logger.trace(Thread.currentThread().getName() + " reading: " + integerBlockingQueue.take() + "\n");
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        },"T2");
        writeThread.start(); readThread.start();
    }
}

